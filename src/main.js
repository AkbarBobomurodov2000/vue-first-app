import {createApp} from 'vue';

import App from './App.vue';
import FreindContact from './components/FreindContact.vue';
import NewFreind from './components/NewFreind.vue';
const app = createApp(App);

app.component('freind-contact', FreindContact);

app.component('new-freind', NewFreind)

app.mount('#app');